package tadeln.tag.saveeditor;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.awt.*;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

public class App {
    private static App instance = null;

    public static App get() {
        if (instance == null) {
            instance = new App();
        }
        return instance;
    }

    public static final String versionString = "0.1.0";

    public static final String baseTitle = "TAG Save Editor v" + versionString;

    private Stage primaryStage;

    private GameSave gameSave;

    private String filename;

    private boolean modified;

    public boolean isModified() {
        return this.modified;
    }

    public void setModified(boolean modified) {
        if (this.modified != modified) {
            this.modified = modified;
            this.updateTitle();
        }
    }

    public void updateTitle() {
        StringBuilder sb = new StringBuilder();
        sb.append(baseTitle);
        if (filename != null) {
            sb.append(" - ");
            sb.append(filename);
        } else if (gameSave != null) {
            sb.append(" - New File");
        }
        if (modified) {
            sb.append("*");
        }
        if (primaryStage != null) {
            primaryStage.setTitle(sb.toString());
        }
    }

    public void init(Stage primaryStage) {
        this.primaryStage = primaryStage;
        primaryStage.setOnCloseRequest(event -> {
            this.quit();
            event.consume();
        });
        this.updateTitle();
    }



    public void quit() {
        if (Controller.getInstance().askToSave("Are you sure you want to quit?")) {
            Platform.exit();
        }
    }



    public void newFile() {
        System.out.println("Creating new savefile");
        gameSave = new GameSave();
        filename = null;
        setModified(true);
        Controller.getInstance().updateFilenameLabel(null);
        Controller.getInstance().refreshVariables();
    }
    public void loadFile(File file) {
        if (file == null) return;
        System.out.println("Loading from file " + file.getAbsolutePath());
        try {
            gameSave = GameSave.loadFromStream(new DataInputStream(new FileInputStream(file)));
            setModified(false);
            filename = file.getAbsolutePath();
            Controller.getInstance().updateFilenameLabel(file.getAbsolutePath());
            Controller.getInstance().refreshVariables();
        } catch (Exception e) {
            alertException("Error while loading file \"" + file.getAbsolutePath() + "\"!", e);
        }
    }
    public void saveFile(File file) {
        if (file == null) return;
        try {
            DataOutputStream stream = new DataOutputStream(new FileOutputStream(file));
            gameSave.writeToStream(stream);
            setModified(false);
            filename = file.getAbsolutePath();
            Controller.getInstance().updateFilenameLabel(file.getAbsolutePath());
        } catch (Exception e) {
            alertException("Error while saving file \"" + file.getAbsolutePath() + "\"!", e);
        }
    }
    public void closeFile() {
        gameSave = null;
        filename = null;
        setModified(false);
        Controller.getInstance().updateFilenameLabel(null);
        Controller.getInstance().refreshVariables();
    }




    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public GameSave getGameSave() {
        return gameSave;
    }

    public String getFilename() {
        return filename;
    }

    public void info(String header, String content) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(baseTitle);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.setResizable(true);

        alert.showAndWait();
    }

    public void about() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(baseTitle);
        alert.setHeaderText("About");
        alert.setContentText("Open Source \"Treasure Adventure Game\" save editor, made in JavaFX\n" +
                "Version: " + versionString + "\n" +
                "Author: TadeLn\n" +
                "2021\n" +
                "\n" +
                "For more info about the variables and values, check out NOTES.md");
        alert.setResizable(true);


        DialogPane d = alert.getDialogPane();

        ButtonType sourceCodeBtn = new ButtonType("Source Code", ButtonBar.ButtonData.YES);
        d.getButtonTypes().add(sourceCodeBtn);
        d.setMinHeight(Region.USE_PREF_SIZE);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get().getButtonData() == ButtonBar.ButtonData.YES) {
                if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {

                    Thread thread = new Thread(() -> {
                        try {
                            Desktop.getDesktop().browse(new URI("https://gitlab.com/TadeLn/tag-save-editor"));
                        } catch (IOException | URISyntaxException ex) {
                            ex.printStackTrace();
                        }
                    });
                    thread.start();

                } else {
                    System.err.println("Browser not supported");
                }
            }
        }
    }

    public void alertException(String message, Exception ex) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error!");
        alert.setHeaderText("An exception occurred");
        if (ex.getMessage() == null) {
            alert.setContentText(message);
        } else {
            alert.setContentText(message + "\n" + ex.getMessage());
        }
        alert.setResizable(true);

        // Create expandable Exception.
        Label label = new Label("The exception stacktrace was:");

        StringBuilder sb = new StringBuilder();
        for (StackTraceElement s : ex.getStackTrace()) {
            sb.append(s.toString());
            sb.append('\n');
        }

        TextArea textArea = new TextArea(sb.toString());
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setExpandableContent(expContent);

        alert.showAndWait();
    }
}
