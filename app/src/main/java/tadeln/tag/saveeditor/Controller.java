package tadeln.tag.saveeditor;

import com.sun.javafx.runtime.VersionInfo;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.TreeMap;

public class Controller implements Initializable {

    private static Controller instance;

    public static Controller getInstance() {
        return instance;
    }


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        instance = this;
        updateFilenameLabel("none");
        refreshVariables();
    }


    @FXML
    private ScrollPane variableContainer;

    @FXML
    private Label filenameLabel;



    @FXML
    private void quit() {
        App.get().quit();
    }



    @FXML
    private void newFile() {
        if (this.askToSave("Are you sure you want to create a new file?")) {
            App.get().newFile();
        }
    }

    @FXML
    private void openFile() {
        if (this.askToSave("Are you sure you want to open another file?")) {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open file");
            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter("Save files (*.dat)", "*.dat")
            );
            File file = fileChooser.showOpenDialog(App.get().getPrimaryStage());

            try {
                App.get().loadFile(file);
            } catch (Exception e) {
                App.get().alertException("Couldn't load from file \"" + App.get().getFilename() + "\"!", e);
            }
        }
    }

    @FXML
    private void reloadFile() {
        String filename = App.get().getFilename();
        if (this.askToSave("Are you sure you want to reload the file?")) {
            if (filename == null) {
                this.openFile();
                return;
            }
            try {
                File file = new File(filename);
                App.get().loadFile(file);
            } catch (Exception e) {
                App.get().alertException("Couldn't load from file \"" + filename + "\"!", e);
            }
        }
    }

    @FXML
    private void saveFile() {
        if (App.get().getFilename() == null) {
            this.saveFileAs();
            return;
        }
        try {
            File file = new File(App.get().getFilename());
            App.get().saveFile(file);
        } catch (Exception e) {
            App.get().alertException("Couldn't save to file \"" + App.get().getFilename() + "\"!", e);
        }
    }

    @FXML
    private void saveFileAs() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save file");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Save files (*.dat)", "*.dat")
        );
        File file = fileChooser.showSaveDialog(App.get().getPrimaryStage());

        try {
            App.get().saveFile(file);
        } catch (Exception e) {
            App.get().alertException("Couldn't save to file \"" + App.get().getFilename() + "\"!", e);
        }
    }

    @FXML
    public void closeFile() {
        if (this.askToSave("Are you sure you want to close the file?")) {
            App.get().closeFile();
        }
    }

    @FXML
    private void exportCSV() {
        if (App.get().getGameSave() == null) {
            App.get().info("No file loaded", "Drag and drop a file onto the window to load a file");
            return;
        }

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export file");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv")
        );
        File file = fileChooser.showSaveDialog(App.get().getPrimaryStage());

        if (file != null) {
            try {
                App.get().getGameSave().writeCSVToFile(file);
            } catch (Exception e) {
                App.get().alertException("Couldn't save to file \"" + file.getAbsolutePath() + "\"!", e);
            }
        }
    }

    public boolean askToSave(String message) {
        if (App.get().getGameSave() == null) {
            return true;
        }
        if (App.get().isModified()) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle(App.baseTitle);
            alert.setContentText("The file is not saved. " + message);
            alert.setResizable(true);


            DialogPane d = alert.getDialogPane();
            d.setMinHeight(Region.USE_PREF_SIZE);

            Optional<ButtonType> res = alert.showAndWait();
            if (res.isPresent()) {
                if (res.get().getButtonData() == ButtonBar.ButtonData.OK_DONE) {
                    return true;
                }
            }
            return false;

        } else {
            return true;
        }
    }



    @FXML
    private void fileDragOver(DragEvent event) {
        if (event.getGestureSource() != variableContainer && event.getDragboard().hasFiles()) {
            event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        event.consume();
    }

    @FXML
    private void fileDragDrop(DragEvent event) {
        Dragboard db = event.getDragboard();
        boolean success = false;
        if (db.hasFiles()) {
            App.get().loadFile(db.getFiles().get(0));
            success = true;
        }
        event.setDropCompleted(success);
        event.consume();
    }


    @FXML
    private void about() {
        try {
            App.get().about();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    @FXML
    private void buttonRefreshVariables() {
        refreshVariables();
        if (App.get().getGameSave() == null) {
            App.get().info("No file loaded", "Drag and drop a file onto the window to load a file");
        }
    }


    public void refreshVariables() {
        int row = 0;
        GridPane gridPane = new GridPane();
        if (App.get().getGameSave() != null) {
            try {
                TreeMap<String, Pair<String, Integer>> variables = App.get().getGameSave().getVariables();
                for (Map.Entry<String, Pair<String, Integer>> entry : variables.entrySet()) {

                    Label label = new Label(entry.getKey());

                    TextField stringTextField = new TextField(entry.getValue().first);
                    stringTextField.textProperty().addListener(
                            (observable, oldValue, newValue) -> {
                                App.get().setModified(true);
                                variables.get(entry.getKey()).first = newValue;
                            }
                    );

                    String numberStr = "";
                    if (entry.getValue().second != null) numberStr = entry.getValue().second.toString();

                    TextField numberTextField = new TextField(numberStr);
                    numberTextField.textProperty().addListener((observable, oldValue, newValue) -> {
                        App.get().setModified(true);
                        try {
                            Integer value = null;
                            if (!newValue.isEmpty()) {
                                value = Integer.parseInt(newValue);
                            }
                            variables.get(entry.getKey()).second = value;
                            numberTextField.getStyleClass().removeAll("invalid");
                        } catch (NumberFormatException e) {
                            numberTextField.getStyleClass().add("invalid");
                        }
                    });

                    Button deleteButton = new Button("Delete");
                    deleteButton.setOnAction(event -> {
                        App.get().setModified(true);
                        variables.remove(entry.getKey());
                        refreshVariables();
                    });

                    gridPane.add(label, 0, row, 1, 1);
                    gridPane.add(stringTextField, 1, row, 1, 1);
                    gridPane.add(numberTextField, 2, row, 1, 1);
                    gridPane.add(deleteButton, 3, row, 1, 1);

                    row++;
                }

            } catch (Exception e) {
                App.get().alertException("Couldn't refresh variables", e);
            }
        }

        gridPane.setPadding(new Insets(10));
        gridPane.setHgap(10);
        gridPane.setVgap(2);

        TextField nameTextField = new TextField();

        Button addButton = new Button("Add");
        addButton.setOnAction(event -> {
            if (App.get().getGameSave() == null) {
                App.get().info("No file loaded", "Drag and drop a file onto the window to load a file");
            } else {
                TreeMap<String, Pair<String, Integer>> variables = App.get().getGameSave().getVariables();
                if (nameTextField.getText() != "") {
                    App.get().setModified(true);
                    variables.put(nameTextField.getText(), new Pair<String, Integer>("", 0));
                    refreshVariables();
                }
            }
        });
        
        gridPane.add(nameTextField, 0, row, 1, 1);
        gridPane.add(addButton, 1, row, 1, 1);
        
        variableContainer.setContent(gridPane);
    }

    public void updateFilenameLabel(String string) {
        if (string == null) {
            filenameLabel.setText("Currently loaded file: none");
        } else {
            filenameLabel.setText("Currently loaded file: " + string);
        }
        App.get().updateTitle();
    }
}
