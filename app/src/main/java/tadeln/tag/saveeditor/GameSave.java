package tadeln.tag.saveeditor;

import java.io.*;
import java.util.Map;
import java.util.TreeMap;

public class GameSave {

    private final TreeMap<String, Pair<String, Integer>> variables;

    public TreeMap<String, Pair<String, Integer>> getVariables() {
        return variables;
    }



    public static GameSave loadFromStream(DataInputStream input) throws IOException {
        GameSave result = new GameSave();

        while (true) {
            char currentChar;

            int length;
            try {
                StringBuilder lengthStr = new StringBuilder();
                currentChar = (char) input.readByte();
                while (Character.isDigit(currentChar)) {
                    lengthStr.append(currentChar);
                    currentChar = (char) input.readByte();
                }

                length = Integer.parseInt(lengthStr.toString());
            } catch (EOFException e) {
                break;
            }

            int currentPos = 0;

            String key;
            try {
                StringBuilder keySB = new StringBuilder();
                currentChar = (char) input.readByte();
                while (currentChar != 0x00) {
                    keySB.append(currentChar);
                    currentChar = (char) input.readByte();
                    currentPos++;
                }
                key = keySB.toString();
            } catch (EOFException e) {
                break;
            }

            String valueStr;
            try {
                StringBuilder valueSB = new StringBuilder();
                currentChar = (char) input.readByte();
                while (currentChar != 0x00) {
                    valueSB.append(currentChar);
                    currentChar = (char) input.readByte();
                    currentPos++;
                }
                valueStr = valueSB.toString();
            } catch (EOFException e) {
                break;
            }

            Integer valueNum = null;
            try {
                StringBuilder valueSB = new StringBuilder();
                while (currentPos < length) {
                    currentChar = (char) input.readByte();
                    valueSB.append(currentChar);
                    currentPos++;
                }
                if (!valueSB.toString().isEmpty()) {
                    valueNum = Integer.parseInt(valueSB.toString());
                }
            } catch (EOFException e) {
                break;
            }

            if (valueStr.isEmpty()) {
                valueStr = null;
            }

            Pair<String, Integer> value = new Pair<>(valueStr, valueNum);

            result.variables.put(key, value);
            System.out.println("Variable read: " + key + " = " + value);
        }
        return result;
    }

    public void writeToStream(DataOutputStream output) throws IOException {
        for (Map.Entry<String, Pair<String, Integer>> entry : variables.entrySet()) {

            String strValue = "";
            if (entry.getValue().first != null) strValue = entry.getValue().first;

            String numValue = "";
            if (entry.getValue().second != null) numValue = entry.getValue().second.toString();

            output.writeBytes(String.valueOf(entry.getKey().length() + strValue.length() + numValue.length()));
            output.writeByte(' ');
            output.writeBytes(entry.getKey());
            output.writeByte(0x00);
            output.writeBytes(strValue);
            output.writeByte(0x00);
            output.writeBytes(numValue);

            System.out.println("Variable written: " + entry.getKey() + " = " + entry.getValue());
        }
    }

    public void writeCSVToFile(File file) throws FileNotFoundException {
        PrintWriter output = new PrintWriter(new FileOutputStream(file));

        for (Map.Entry<String, Pair<String, Integer>> entry : variables.entrySet()) {
            output.print(entry.getKey());
            output.print(',');
            if (entry.getValue().first != null) {
                output.print(entry.getValue().first);
            }
            output.print(',');
            if (entry.getValue().second != null) {
                output.print(entry.getValue().second);
            }
            output.print('\n');

            System.out.println("(CSV) Variable written: " + entry.getKey() + " = " + entry.getValue());
        }
        output.flush();
    }



    public GameSave() {
        variables = new TreeMap<>();
    }

}
