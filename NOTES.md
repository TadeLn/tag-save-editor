# Save File Notes

A save file consists of 4 types of variables: Global Values, Collectables, sometimes other values and a save point frame. 
The first three are listed below, and the last one contains the ID of the level that the save was made in.
Level IDs are also listed below.

Each variable has a name and 2 values, the first one is a string of text, and the second one is always a number.

The document is not complete, some of the values are missing. If you want to, you can contribute to this project!

(?) means unsure / unconfirmed / missing information / todo

## Format of the .dat file

(Technical explanation, skip this section if you aren't interested)

Here is a fragment of an example .dat save file:
```
34 33 20 63  68 65 73 74  20 32 30 20  63 6F 69 6E  43 chest 20 coin
73 31 39 37  37 31 30 32  00 43 68 65  73 74 20 32  s1977102.Chest 2
30 20 43 6F  69 6E 73 31  39 37 37 31  30 32 00 31  0 Coins1977102.1
34 35 20 63  68 65 73 74  20 32 30 20  63 6F 69 6E  45 chest 20 coin
73 32 30 32  33 30 31 35  30 00 43 68  65 73 74 20  s20230150.Chest
```
(on the left there are bytes in hexadecimal,
on the right there is a text representation of those bytes; null bytes are represented as dots `.`)

To read the values from the file you have to:
1. Read the length of the entry (you look at the number in text form, and read it until there is a space).
   
   In this example it would be 43
   
2. Read the name of the variable until you encounter a null byte.
   
   In this example `chest 20 coins1977102`
   
3. Read the string value of the variable until you encounter a null byte.
   
   In this example `Chest 20 coins1977102`
   
4. Read the next *x* bytes in text form and convert to a number, where *x* = `length of entry - (length of name string + length of value string)` 

   In this example, *x* = 43 - (21 + 21) = 1, so we just read 1 character.
   This character is `1`, so our number value is `1`
   
5. Repeat the process from step 1 until EOF

## Global Values

Global Values contain general information about the story progression and the player's inventory.
Things like current time, coin count, collected treasures and selected hat are stored here.

Global Values' names are always in the format:
`global value <n>` where `<n>` is a whole number.
Global Values never have a string value, they only use the number value. 

Format:
- `[globalValueNumber]` Description of the Global Value, numbers below are possible values:
    - (value1) value description
    - (value2) value description
    
---

- `[1]` Currently Equipped Item
    - (0-2) nothing (?)
    - (3) Magic Bottle
    - (>3) nothing (?)
  
- `[2]` Coin Count (display maxes out at 9999, actual value can be over 9999)
- `[3]` Health / Max Health (?) (recalculated at file load)

- `[6]` Held sprite ID (?)
    - (40) Apple
  
- `[7]` Equipped Hat:
    - (0) no hat
    - (1) Paper Hat
    - (2) Explorer Hat
    - (3) Top Hat
    - (4) New Wave Hat
    - (5) Baseball Hat
    - (6) Mouse Hat
    - (>6) no hat

  Works even if the hat is not in the inventory.

- `[8]` Selected Map:
    - (0) no selected map
    - (1) Museum Map
    - (2) Sail Map
    - (3) Magic Bottle Map
    - (4) Cannon Map
    - (5) Dive Helmet Map
    - (6) Flashlight Map
    - (7) Treasure Map 1
    - (8) Treasure Map 2
    - (9) Treasure Map 3
    - (10) Treasure Map 4
    - (11) Treasure Map 5
    - (12) Treasure Map 6
    - (13) Treasure Map 7
    - (14) Treasure Map 8
    - (15) Treasure Map 9
    - (16) Treasure Map 10
    - (17) Treasure Map 11
    - (18) Treasure Map 12
    - (>18) no selected map

  Works even if the map is not in the inventory.

- `[9]` Magic Bottle contents
    - (0) nothing
    - (1) Fire
    - (2) Air
    - (3) Water
    - (4) Bubble
    - (5) nothing; glitched, can't swing the hook or use the bottle
    - (6-7) ???; glitched, can't rotate the bottle, using does nothing (?)
    - (8) Warp Dimension portal
    - (9) Lazer
    - (10) Wakanda's Magic
    - (>10) same as (5)

- `[10]` Shovel // This value increments file progress by `thisValue / 57 * 100%;`
- `[11]` Throwing Hook // This value increments file progress by `thisValue / 57 * 100%;`
- `[12]` Sail
    - (0) no sail
    - (1) Sail
    - (2) Sail + Flag
    - (>2) Sail + Flag (works as normal; inventory shows empty slot, but when the slot is selected, the text changes to "Sail + Flag")

  This value increments file progress by `thisValue / 57 * 100%;`

- `[13]` Cannon
    - (0) no cannon
    - (1) Cannon
    - (2) Torpedo Cannon
    - (>2) Torpedo Cannon (works as normal; inventory shows empty slot)

  This value increments file progress by `thisValue / 57 * 100%;`

- `[14]` Parrot
    - (0) no parrot
    - (1) Parrot
    - (>1) Parrot (works as normal; inventory shows a darker parrot; when the slot is selected, the text is "Parrot" instead of "Parrot (UP + ACTION)")

  This value increments file progress by `thisValue / 57 * 100%;`

- `[15]` Dive Helmet // This value increments file progress by `thisValue / 57 * 100%;`

- `[17]` Magic Bottle // This value increments file progress by `thisValue / 57 * 100%;`
- `[18]` Compass:
    - (0) no compass
    - (1) Compass
    - (2) Temporal Compass
    - (>2) Temporal Compass (works as normal; inventory shows empty slot, but when the slot is selected, the text changes to "Temporal Compass")

  This value increments file progress by `thisValue / 57 * 100%;`

- `[21]` Warp World X position, possibly used for other things

- `[23]` Current state ("carry flag")
    - (0) normal
    - (1) (?)
    - (2) carrying something heavy (rock)
    - (3) carrying something light (apple)
    - (>3) normal (?)

// All values below increment file progress by `thisValue / 57 * 100%;`
- `[27]` Museum Map
- `[28]` Sail Map
- `[29]` Magic Bottle Map
- `[30]` Cannon Map
- `[31]` Dive Helmet Map
- `[32]` Flashlight Map
- `[33]` Treasure Map 1
- `[34]` Treasure Map 2
- `[35]` Treasure Map 3
- `[36]` Treasure Map 4
- `[37]` Treasure Map 5
- `[38]` Treasure Map 6
- `[39]` Treasure Map 7
- `[40]` Treasure Map 8
- `[41]` Treasure Map 9
- `[42]` Treasure Map 10
- `[43]` Treasure Map 11
- `[44]` Treasure Map 12
- `[45]` Heart 1
- `[46]` Heart 2
- `[47]` Heart 3
- `[48]` Heart 4
- `[49]` Heart 5
- `[50]` Heart 6
- `[51]` Paper Hat
- `[52]` Explorer Hat
- `[53]` Top Hat
- `[54]` New Wave Hat
- `[55]` Baseball Hat
- `[56]` Mouse Hat
- `[57]` Orb of Revelation
- `[58]` Temporal Ring
- `[59]` Book of Rendition
- `[60]` Echo Mirror
- `[61]` Gem of Rapid Oxidation
- `[62]` Gossamer Cane
- `[63]` Sword of Vim
- `[64]` Chaos Whistle
- `[65]` Incessant Potion
- `[66]` Cloak of Inconspicuousness
- `[67]` Voltaic Bracelet
- `[68]` Casimir Feather
  // All values above increment file progress by `thisValue / 57 * 100%;`

- `[69]` Time - Hour
  (0-20) Valid time values
  (>20) Resets to 0

- `[71]` Grandma Museum Map Quest Progress
    - (0) Intro Island cutscene
      `save point frame = 8`: normal cutscene;
      any other savepoint: ~5 sec. black fade-in after spawning in; when entering level 8, a glitched version of the cutscene plays
    - (1) Intro cutscene: first dialogue line
      `save point frame = 8`: softlocks, because you spawn in AngelOak, and the character cannot get into the boat
      any other savepoint: ~5 sec. black fade-in after spawning in; when entering level 8, a glitched version of the boat cutscene plays
    - (2) Same as above, with no black fade-in
    - (3) Intro cutscene (glitched, can't progress)
    - (4) Intro cutscene: When entering level 8: 1 sec. of delay, then increments to the next value
    - (5) Intro cutscene: When entering level 8: getting into the boat - the character moves automatically, but you can move anyway
    - (6) Intro cutscene: When entering level 8: 1 sec. of delay, then increments to the next value
    - (7) Intro cutscene: When entering level 8: getting into the boat - Grandma's dialogue
    - (8) Intro cutscene (glitched, can't progress)
    - (9) Intro cutscene: When entering level 8, increments to the next value
    - (10) After Intro cutscene

    - (14) After talking to Grandma Ethor for the map
    - (15) After getting the Museum Map
    - (16) After talking to Grandpa Ethor // If value is over 15, the Unknown Map is renamed to Museum Map
    - (17) After talking to Baggus for the first time
  
- `[82]` Lockpick Kit
  - (0) no lockpick kit
  - (1) Lockpick Kit
  - (2) Lockpick Kit + Keycard
  - (>2) no lockpick kit (?)
  
- `[84]` Health / Max Health (?) (recalculated at file load)

- `[139]` Interdimensional Fragment Count



## Collectables

Collectables are things that can be collected only once (placed coins, chests, interdimensional fragments).
They are saved in the 

### Chests
- `Chest 20 Coins1977102` - Cargale Island - Chest in 6-15 store

### Interdimensional fragments
- `Collectable8764876` - AngelOak Island - in the tree above Trevor
- `Collectable2063546` - Cargale Island - inside the cave



## Other variables

There are also other variables that aren't global values or collectables.
These are rarely used and not many of them are known.

- `dark room plant` - (?)
- `gear cave door` - (?)
- `greenhouse plant` - Unknown, may be related to AngelOak growable plant (?)
- `matties shop door` - (?)
- `mush torch 1` - (?)
- `mush torch 2` - (?)
- `mush torch 3` - (?)
- `mush torch 4` - (?)
- `mush torch 5` - (?)
- `volcano door` - (?)
- `witches basement door` - (?)



# Levels

## Island name cheatsheet

- AngelOak - Big Tree Island
- Boracay - Jungle Island 2 (with Crystal Caves)
- Cargale - Starting Island
- Cologel - Throwing Hook Island
- Danausel - Modern City
- Eber - Magic Bottle Island
- Jatko - Dive Helmet Island
- Nimue - Museum/Temple Island
- Pinon - Treasure Map 3 Island
- Poveglia - Haunted Island
- Solfor - Volcanic Island
- Somora - Underground Worm City
- Tenerife - Desert Island
- Tomba - Mushroom Island
- Zwaan - Jungle Island 1

## Level IDs

Level IDs are used in the `save point frame` variable.
Every level has at most one savepoint, so there is no additional information about position / checkpoint ID.

- `[0]` doesn't load
- `[1]` Title Screen (positions window in the center of the screen)
- `[2]` **Debug Screen**
- `[3]` Title Screen
- `[4]` Eber Island (-891, 3) (OverWorld 1)
- `[5]` Tomba Island (-1006, 28) (OverWorld 1 Sky)
- `[6]` ice bits (-735, 0) (OverWorld 2)
- `[7]` Poveglia Island (-535, 45) (OverWorld 2 Sky)
- `[8]` AngelOak Island (190, 4) (OverWorld 3)
- `[9]` AngelOak Island (169, 70) (OverWorld 3 Sky)
- `[10]` Tenerife Island (481, 4) (Overworld 4)
- `[11]` Solfor Island (758, 59)
- `[12]` Zwaan Island (907, 19) (OverWorld 5)
- `[13]` Boracay Island (1019, 34) (OverWorld 5 Sky)
- `[14]` Nimue Island - Museum
- `[15]` Cargale Island - Grandma Ethor's house
- `[16]` Cargale Island - above the table in Brooks' and Barney's house
- `[17]` Cargale Island - above the table in Mayor Goodrich's house
- `[18]` Cargale Island - below the chimney enterance in Joe and Adrienne's house
- `[19]` Cargale Island - 6-15 store; left edge of the platform with the hat stand
- `[20]` Cargale Island - cave; 3rd sand tile from the right wall
- `[21]` Somora Island - underground - savepoint after first gate
- `[22]` Somora Island - underground - savepoint before main area
- `[23]` Somora Island - underground - first weight elevator; ~2 tiles to the right of the fire torch
- `[24]` Somora Island - underground - Mother-Father's room; ~0.5 tiles to the left of the center of the first drop
- `[25]` Somora Island - underground - Olli's platforming area; on the cash bag
- `[26]` Somora Island - underground - between the left wall and the left hatstand in the hat store
- `[27]` Somora Island - underground - first blue-cave section; ~2 tiles above the ground and ~1 tile to the left of the first air stream
- `[28]` Somora Island - underground - area ~8 tiles to the right of Kray; before the parrot puzzle
- `[29]` Somora Island - underground - Edward's Lab; ~1 tile above Edward
- `[30]` Somora Island - underground - abandoned pawn shop; left side of the ledge with a water bottle
- `[31]` Somora Island - underground - savepoint in bluecave area after fire
- `[32]` Somora Island - underground - upper cave area with meditation thing (glitched, no visible player)
- `[33]` AngelOak Island - tree
- `[34]` Tenerife Island - small pyramid
- `[35]` Tenerife Island - big pyramid (savepoint before bossfight)
- `[36]` Tenerife Island - saloon
- `[37]` Tenerife Island - golem bossfight
- `[38]` Pinon Island - Treasure Map 3 area; ~2 tiles to the left of the green lever, 1 tile up
- `[39]` Cologel Island - Throwing Hook area
- `[40]` Danausel Island - building to the right of Surf+Surf (glitched, no visible player)
- `[41]` Danausel Island - building to the right of City Library
- `[42]` Danausel Island - Housewares etc. building, in front of "Housewares etc." sign
- `[43]` Danausel Island - City Library (with noclip guy, hold left for ~15 sec. and up for ~5 sec.)
- `[44]` Poveglia Island - bottom parrot puzzle cave; between a green spike enemy and a big movable rock
- `[45]` Poveglia Island - ruined green house; 1 tile to the left of the chest
- `[46]` Poveglia Island - Estill's House
- `[47]` Poveglia Island - Lighthouse; 1 tile from the left wall
- `[48]` Poveglia Island - Inside floating island (savepoint before bossfight)
- `[49]` Solfor Island - savepoint inside the cave
- `[50]` Poveglia Island - Inside castle walls; ~2 tiles to the right of the castle enterance 
- `[51]` Poveglia Island - savepoint inside castle
- `[52]` Poveglia Island - Castle dungeon; ~2 tiles to the left of the burnable wall at the end
- `[53]` Poveglia Island - Castle tower; ~1 tile to the right of the door
- `[54]` Eber Island - Magic Bottle Area; on the sand
- `[55]` Danausel Island - Pawn Shop
- `[56]` Danausel Island - Club stage, ~1 tile to the left of the top door
- `[57]` Danausel Island - Club bar, ~1 tile to the left of the top door
- `[58]` Tomba Island - underground area; ~2 tiles to the left of the first carrot
- `[59]` Tomba Island - savepoint by Gogondo
- `[60]` Tomba Island - Cremini's house
- `[61]` Boracay Island - savepoint in the house near Cinder
- `[62]` Cargale Island - sewers
- `[63]` Poveglia Island - S-MART
- `[64]` Danausel Island - Breakrys
- `[65]` Danausel Island - Holocade; to the right of the high score board
- `[66]` Warp Dimension - exact location depends on global values
- `[67]` Diving - x: -1404
- `[68]` Diving - x: -842
- `[69]` Diving - x: -280
- `[70]` Diving - x: 282
- `[71]` Diving - x: 844
- `[72]` Psaul Hallucination level
- `[73]` Psaul Hallucination level heart
- `[74]` Danausel Island - GC Building; ~2 tiles to the left of the elevator
- `[75]` Danausel Island - GC Building, Treasure Map 6 area; ~1 tile to the left of the vent
- `[76]` Danausel Island - GC ventilation system; under the 3 narrow pipes below the exit
- `[77]` Tomba Island - savepoint before the boss
- `[78]` Boracay Island - cave enterance
- `[79]` Boracay Island - 1st crystal cave checkpoint
- `[80]` Boracay Island - 2nd crystal cave checkpoint
- `[81]` Boracay Island - 3rd crystal cave checkpoint
- `[82]` Boracay Island - crystal cave boss area; to the right of the rightmost cracked tile
- `[83]` Boracay Island - crystal cave treasure checkpoint
- `[84]` Boracay Island - crystal cave minecart ride (doesn't load properly)
- `[85]` Minecart Ride - Ocean tunnel to GC Factory; on the GC Drillo instructions 
- `[86]` Danausel Island - sewers
- `[87]` Danausel Island - GC hidden building
- `[88]` Danausel Island - GC factory
- `[89]` Danausel Island - GC factory pirate bossfight
- `[90]` Danausel Island - GC factory mainframe bossfight
- `[91]` Somora Island - left house; ~1 tile to the left of the globe
- `[92]` Danausel Island - GC factory mainframe post bossfight area; ~1 tile to the right of the door
- `[93]` House for sale; to the left of the campfire
- `[94]` Nimue Island - Final Temple
- `[95]` Nimue Island - Final Temple flashback
- `[96]` Louyang Battle Phase 1 (Warp Dimension)
- `[97]` Louyang Battle Phase 2 (Super Mario Bros.)
- `[98]` Louyang Battle Phase 3 (Kirby)
- `[99]` Louyang Battle Phase 4 (Castlevania)
- `[100]` Louyang Battle Final
- `[101]` Credits
- `[102]` Intro - Story
- `[103]` Intro - Journey
- `[104]` Intro - Waking up
- `[105]` Save select screen
- `[106]` Game over screen
- `[107]` Controls screen
- `[108]` About screen
- `[>108]` About screen

## Debug Screen

Level ID 2 is filled with overlapping text, so all of the avaliable options are listed below.

Numbers in square brackets `[]` are Level IDs.

### Locations

- `[4]` OverWorld 1 - Eber Island (-891, 3)
- `[5]` OverWorld 1 Sky - Tomba Island (-1006, 28)
- `[6]` OverWorld 2 - ice bits (-735, 0)
- `[7]` OverWorld 2 Sky - Poveglia Island (-535, 45)
- `[8]` OverWorld 3 - AngelOak Island (190, 4)
- `[9]` OverWorld 3 Sky - AngelOak Island (169, 70)
- `[10]` Overworld 4 - Tenerife Island (481, 4)

- `[12]` OverWorld 5 - Zwaan Island (907, 19)
- `[13]` OverWorld 5 Sky - Boracay Island (1019, 34)
- `[14]` Museu - Museum

- `[66]` Warp Room - Warp Dimension (location depends on global values)
- `[58]` Mushroom Cave - underground area; ~2 tiles to the left of the first carrot
- `[50]` Castle - Inside castle walls; ~2 tiles to the right of the castle enterance
- `[76]` Ventilation - GC ventilation system; under the 3 narrow pipes below the exit
- `[79]` Crystal Cave - Boracay Island - 1st crystal cave checkpoint
- `[86]` City Sewer - Danausel Island - sewers
- `[87]` GC Building - GC hidden building, after the sewers (location - door to the sewers)
- `[90]` Computer Boss - GC Factory, before the GC Mainframe
- General Store Somora - General Store in Somora (location - on exit door) (?)
- `[94]` Temple - (hard to click) Final temple area, just before final boss (location - on enterance door) 

- `[32]` Elevator Room - Upper Somora underground area (location - glitched, no visible player)
- `[72]` Trippin - Psaul Hallucination level
- `[37]` Golem Boss - Orb of Revelation boss (location - savepoint after the boss)
- `[33]` Inner Tree - AngelOak tree
- `[22]` Somora - Somora main underground area (location - underground exit door)
- `[10]` Desert - Tenerife Island, Saloon enterance (573, 6)
- `[35]` Big Pyramid - Inside of the big pyramid on Tenerife Island (617, 12) (location - enterance)
- `[4]` City - Danausel Island - Pawn Shop enterance (-1241, 4)
- `[49]` Volcan - Solfor Island - savepoint inside the cave (747, 3)
- `[67]` Underwater - Leftmost underwater area (-1395, -25)
- MineCart - Crystal Caves; area just before minecart ride (location - savepoint before minecart ride)
- Pirates Boss 1 - Cologel Island - Enterance to the throwing hook area (383, 6)
- Pirates Boss 2 - GC Factory, Pirate Bossfight Room (location - in front of the enterance)
- Catacombs - Inside catacombs (-542, 55) (location - enterance to catacombs)
- Final CutScenes - Final Temple area (location - enterance)
- Final Boss - Start Louyang bossfight (location - ???)
- Holocade - Start Breakrys minigame (location - ???)
- Haunted Island - Poveglia Island (-555, 3)
- Jungle - Boracay Island (right in front of the cave enterance) (1154, 21)

- Title Screen - back to title screen

### Equipment

- Hearts 1-6

- Compass / Clock
- Throw Hook
- Sail or Flag
- Dive Helmet
- Magic Bottle
- Cannon / Torpedo
- LockPick Kit
- Shovel
- Parrot
- Boots
- Flashlight

### Modifiable values

- Time
- Coins
- Lockpick
