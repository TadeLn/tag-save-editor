
# Treasure Adventure Game Save Editor

Treasure Adventure Game Save Editor is a JavaFX application made to... edit saves of "Treasure Adventure Game"

Treasure Adventure Game: http://robitgames.com/treasure-adventure-game/

## Installation

1. Download a .jar from the Releases section.
2. Download and install Java 11 for your operating system from [here]( https://adoptopenjdk.net/), and intstall / extract it.
3. Go into the `bin` folder inside the extracted JDK folder. (On Mac it's located in `<jdkFolder>/Content/Home/bin/`)
4. Open the command line in the folder (on Windows: Shift + Right Click on the background of the folder), and run `./java -jar <pathToJar>` (or `.\java.exe -jar <pathToJar>` on Windows), where `<pathToJar>` should be replaced with the downloaded .jar file of the application.
5. The application should now launch.

## Usage

1. Get a save file from Treasure Adventure Game. (with .dat extension)
2. Create a backup of it.
3. Run the application, and select `File > Load`. (or press `Ctrl+O`) (or drag the file onto the window)
4. Edit some values.
5. Save the file `File > Save`. (or press `Ctrl+S`)
6. Move/copy the file to the Treasure Adventure Game folder, and load the save in game.

Check out NOTES.md for more info about the variables
